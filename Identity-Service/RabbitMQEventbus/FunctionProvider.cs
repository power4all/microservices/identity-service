﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Identity_Service.Data;
using Identity_Service.Models.DTO;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client.Events;
using RabbitMQEventbus.Extension.FunctionProvider;
using RabbitMQEventbus.Extension.Message;
namespace Identity_Service.RabbitMQEventbus
{
    public class FunctionProvider : RabbitMQFunctionProviderBase
    {
        private readonly ILogger<FunctionProvider> _logger;
        private readonly IServiceProvider _serviceProvider;

        public FunctionProvider(ILogger<FunctionProvider> logger, IServiceProvider serviceProvider)
        {
            _logger = logger;
            _serviceProvider = serviceProvider;
        }

        protected override Dictionary<string, Func<object, BasicDeliverEventArgs, RabbitMQMessage>> InializeFunctionsWithKeys()
        {
            var dictionary = new Dictionary<string, Func<object, BasicDeliverEventArgs, RabbitMQMessage>>();
            dictionary.Add(nameof(HandleRemoveAccount), HandleRemoveAccount);

            return dictionary;
        }

        private RabbitMQMessage HandleRemoveAccount(object o, BasicDeliverEventArgs eventArgs)
        {
            var body = eventArgs.Body.ToArray();
            var message = Encoding.UTF8.GetString(body);

            try
            {
                var deleteAccountMessage = Newtonsoft.Json.JsonConvert.DeserializeObject<DeleteAccountMessage>(message);

                using (var scope = _serviceProvider.CreateScope())
                {
                    var context = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();

                    var account = context.Users.FirstOrDefault(x => x.Id == deleteAccountMessage.UserId.ToString());

                    context.Users.Remove(account);

                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                _logger.LogWarning(e.StackTrace);
            }

            return null;
        }
    }
}
