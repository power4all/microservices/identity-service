﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Identity_Service.Models.DTO
{
    public class DeleteAccountMessage
    {
        public Guid UserId { get; set; }
    }
}
