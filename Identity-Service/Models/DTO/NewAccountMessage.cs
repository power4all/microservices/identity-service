﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Identity_Service.Models.DTO
{
    public class NewAccountMessage
    {
        public Guid UserId { get; set; }
        public string Email { get; set; }
    }
}
