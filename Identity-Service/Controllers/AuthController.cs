﻿using Identity_Service.AccountConfig;
using Identity_Service.Models;
using Identity_Service.Models.DTO;
using IdentityServer4;
using IdentityServer4.Events;
using IdentityServer4.Extensions;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using RabbitMQEventbus.Extension.Eventbus;
using RabbitMQEventbus.Extension.Message;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Identity_Service.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	[AllowAnonymous]
	public class AuthController : ControllerBase
	{
		private readonly RoleManager<IdentityRole> roleManager;
		private readonly UserManager<AppUser> userManager;
		private readonly IEventService eventService;
		private readonly SignInManager<AppUser> signInManager;
		private readonly IRabbitMQEventbus _eventbus;

        public AuthController(RoleManager<IdentityRole> roleManager, UserManager<AppUser> userManager, IEventService eventService, SignInManager<AppUser> signInManager, IRabbitMQEventbus eventbus)
        {
            this.roleManager = roleManager;
            this.userManager = userManager;
            this.eventService = eventService;
            this.signInManager = signInManager;
            _eventbus = eventbus;
        }

        [HttpPost("Register")]
		public async Task<IActionResult> Register([FromBody] RegisterModel model)
		{
			if (!ModelState.IsValid) return BadRequest(model);

			var user = new AppUser { UserName = model.Email, Email = model.Email };

			var result = await userManager.CreateAsync(user, model.Password);

			string role = "Basic_User";

			if (!result.Succeeded) return Conflict(result.Errors);

			if (await roleManager.FindByNameAsync(role) == null)
			{
				await roleManager.CreateAsync(new IdentityRole(role));
			}

			await userManager.AddToRoleAsync(user, role);
			await userManager.AddClaimAsync(user, new Claim("userName", user.UserName));
			await userManager.AddClaimAsync(user, new Claim("email", user.Email));
			await userManager.AddClaimAsync(user, new Claim("role", role));

			var message = new NewAccountMessage()
			{
				UserId = Guid.Parse(user.Id),
				Email = user.Email,
			};

			var json = Newtonsoft.Json.JsonConvert.SerializeObject(message);
			_eventbus.Publish(new RabbitMQMessage(new MessageDestination("AccountsExchange", "accounts.new"), json));

			return Created("api.lifelinks.nl/api/account/register", user);
		}

		[Authorize]
		[HttpPost("Register/poweruser")]
		public async Task<IActionResult> RegisterPower4AllUser([FromBody] RegisterModel model)
		{
			if (!ModelState.IsValid) return BadRequest(model);

			var user = new AppUser { UserName = model.Email, Email = model.Email };

			var result = await userManager.CreateAsync(user, model.Password);

			string role = "Power4All_User";

			if (!result.Succeeded) return Conflict(result.Errors);

			if (await roleManager.FindByNameAsync(role) == null)
			{
				await roleManager.CreateAsync(new IdentityRole(role));
			}

			await userManager.AddToRoleAsync(user, role);
			await userManager.AddClaimAsync(user, new Claim("userName", user.UserName));
			await userManager.AddClaimAsync(user, new Claim("email", user.Email));
			await userManager.AddClaimAsync(user, new Claim("role", role));

			return Created("api.lifelinks.nl/api/account/register/power4alluser", user);
		}

		[HttpPost("SignIn")]
		public async Task<IActionResult> SignIn([FromBody] LoginModel model)
		{
			var user = await signInManager.UserManager.FindByNameAsync(model.Email);

			if (user == null)
			{
				return BadRequest();
			}

			if ((await signInManager.CheckPasswordSignInAsync(user, model.Password, false)) == Microsoft.AspNetCore.Identity.SignInResult.Failed)
			{
				return BadRequest();
			}

			await eventService.RaiseAsync(new UserLoginSuccessEvent(user.Email, user.Id, user.Email));

			AuthenticationProperties props = null;
			if (AccountOptions.AllowRememberLogin && model.RememberLogin)
			{
				props = new AuthenticationProperties
				{
					IsPersistent = true,
					ExpiresUtc = DateTimeOffset.UtcNow.Add(AccountOptions.RememberMeLoginDuration)
				};
			}

			var issuer = new IdentityServerUser(user.Id)
			{
				DisplayName = user.Email
			};

			await HttpContext.SignInAsync(issuer, props);

			return Ok(issuer);
		}

		[HttpPost("SignOut")]
		public async Task<IActionResult> Logout()
		{
			if (User?.Identity.IsAuthenticated != true)
			{
				return BadRequest();
			}

			await signInManager.SignOutAsync();

			await eventService.RaiseAsync(new UserLogoutSuccessEvent(User.GetSubjectId(), User.GetDisplayName()));

			return SignOut();
		}

		[Authorize]
		[HttpGet("check")]
		public IActionResult CheckAuth()
		{
			var check = User.Identity.IsAuthenticated;
			return Ok(check);
		}
	}
}